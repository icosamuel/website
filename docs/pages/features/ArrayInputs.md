# Array Inputs Elements

To render a [array field](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.4), define your schema like:

**schema.json**

```json
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "arrayInput": {
      "type": "array",
      "items": {
        "type": "string"
      }
    }
  }
}
```

**component.vue**

`vue-json-schema` will render a text input by adding a button to add more inputs.

```html
<script>
  import FormSchema from 'vue-json-schema'

  // To override the default array button props
  FormSchema.setComponent('arraybutton', 'button', {
    native: true, // required to force button rendering as HTML native element
    label: 'Add more item'
  })

  export default { ... }
</script>
```
