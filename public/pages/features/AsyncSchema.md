# Async Schema

To asynchronously load a schema, just set a promise that returns it.

```html
<template>
  <FormSchema :schema="schema"/>
</template>

<script>
  import axios from 'axios'
  import FormSchema from 'vue-json-schema'

  export default {
    data: () => ({
      schema: axios.get('/api/schema/subscription.json'),
    }),
    components: { FormSchema }
  }
</script>
```
