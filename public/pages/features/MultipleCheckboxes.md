# Multiple Checkbox elements

To define multiple checkbox, use the [JSON Schema keyword `anyOf`](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.27):

**schema.json**

```json
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "multipleCheckbox": {
      "type": "array",
      "anyOf": [
        { "value": "daily", "label": "Daily New" },
        { "value": "promotion", "label": "Promotion" }
      ]
    }
  }
}
```

**component.vue**

```html
<script>
  import FormSchema from 'vue-json-schema'

  FormSchema.setComponent('select', 'el-select', ({ item }) => {
    return { label: item.value }
  })

  FormSchema.setComponent('checkboxgroup', 'el-checkbox-group')

  export default { ... }
</script>
```
